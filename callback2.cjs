function boardList(lists,boardId,callback) {
  try {
    setTimeout(() => {
      let listId = Object.keys(lists).find((list) => {
        return list == boardId;
      });
      if (listId) {
        let requiredList = lists[listId];
        callback(requiredList);
      } else {
        callback(null);
      }
    }, 2000);
  } catch (error) {
    console.log(error);
  }
}

module.exports = { boardList };
