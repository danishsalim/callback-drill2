function boardInfo(boards,boardId, callback) {
  try {
    setTimeout(() => {
      const data = boards.find((board) => {
        return board.id == boardId;
      });
      callback(data);
    }, 2000);
  } catch (error) {
    console.log(error);
  }
}

module.exports = { boardInfo };
