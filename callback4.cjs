const boards = require("./data/boards_2.json");
const lists = require("./data/lists_1.json");
const cards = require("./data/cards_2.json");

function getMindList(boardInfo, boardList, cardList) {
  try {
    boardInfo(boards, "mcu453ed", (boardInformation) => {
      console.log(boardInformation);
      boardList(lists, boardInformation.id, (list) => {
        console.log(list);
        let mind = list.find((item) => {
          return item.name == "Mind";
        });
        if (mind) {
          cardList(cards, mind.id, (cardlist) => {
            console.log(cardlist);
          });
        } else {
          return;
        }
      });
    });
  } catch (error) {
    console.log(error);
  }
}

module.exports = { getMindList };
