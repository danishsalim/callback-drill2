function cardList(cards, listId, callback) {
  try {
    setTimeout(() => {
      let id = Object.keys(cards).find((id) => {
        return id == listId;
      });
      if (id) {
        let card = cards[id];
        callback(card);
      } else {
        return;
      }
    }, 2000);
  } catch {
    return;
  }
}

module.exports = { cardList };
