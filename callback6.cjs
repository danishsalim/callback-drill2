const boards = require("./data/boards_2.json");
const lists = require("./data/lists_1.json");
const cards = require("./data/cards_2.json");

function allCardsForAllList(boardInfo, boardList, cardList) {
  try {
    boardInfo(boards, "mcu453ed", (boardData) => {
      console.log(boardData);
      boardList(lists, boardData.id, (list) => {
        console.log(list);
        let cardIds = list.map((item) => {
          return item.id;
        });
        cardIds.forEach((id) => {
          cardList(cards, id, (cardList) => {
            console.log(cardList);
          });
        });
      });
    });
  } catch (error) {
    console.log(error);
  }
}

module.exports = { allCardsForAllList };
