const boards = require("./data/boards_2.json");
const lists = require("./data/lists_1.json");
const cards = require("./data/cards_2.json");

function getInfo(boardInfo, boardList, cardList) {
  try {
    boardInfo(boards, "mcu453ed", (boardInfo) => {
      console.log(boardInfo);
      boardList(lists, boardInfo.id, (list) => {
        console.log(list);
        let mind = list.find((item) => {
          return item.name == "Mind";
        });
        let space = list.find((item) => {
          return item.name == "Space";
        });
        cardList(cards, mind.id, (cardlist) => {
          console.log(cardlist);
        });
        cardList(cards, space.id, (cardlist) => {
          console.log(cardlist);
        });
      });
    });
  } catch (error) {
    console.log(error);
    return;
  }
}

module.exports = { getInfo };
